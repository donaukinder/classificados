<?php require 'config.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Classificados</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  

</head>
<body>
  <nav class="navbar navbar-inverse">
  	<div class="container-fluid">
  		<div class="navbar-header">
  			<a href="./" class="navbar-brand">Classificados</a>
  		</div>
  		<ul class="nav navbar-nav navbar-right">
        <?php if (isset($_SESSION['cLogin']) && !empty($_SESSION['cLogin'])):
        require 'classes/usuarios.class.php';
        ?>
          <li><a><?= Usuarios::getNome($_SESSION['cLogin']);?></a></li>
          <li><a href="meus-anuncios.php">Meus Anúncios</a></li>
          <li><a href="sair.php">Sair</a></li>
        <?php else: ?>
  			 <li><a href="cadastre-se.php">Cadastre-se</a></li>
  			 <li><a href="login.php">Login</a></li>
        <?php endif; ?>
  		</ul>
  	</div>
  </nav>